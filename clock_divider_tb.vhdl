library ieee;
use ieee.std_logic_1164.all;

library work;
use work.config.all;

entity clock_divider_tb is

end entity clock_divider_tb;

architecture rtl of clock_divider_tb is
  signal clk_in  : std_ulogic;
  signal clk_out : std_ulogic;
begin  -- architecture rtl

  clock1 : component clock_mock
    generic map (
      CLOCK_FREQ => CLOCK_FREQ)
    port map (
      clk_out => clk_in);

  clock_divider1 : component clock_divider
    generic map (
      INPUT_FREQ  => CLOCK_FREQ,
      OUTPUT_FREQ => PROJECT_FREQ)
    port map (
      clk_in  => clk_in,
      clk_out => clk_out);

end architecture rtl;

configuration clock_divider_test of clock_divider_tb is

  for rtl
    for clock1 : clock_mock
      use entity work.clock_mock
        generic map (
          CLOCK_FREQ => 200000);
    end for;

    for clock_divider1 : clock_divider
      use entity work.clock_divider
        generic map (
          INPUT_FREQ  => 200000,
          OUTPUT_FREQ => 20000);
    end for;
  end for;

end configuration clock_divider_test;
