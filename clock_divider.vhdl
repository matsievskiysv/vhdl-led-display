library ieee;
use ieee.std_logic_1164.all;

entity clock_divider is
  generic (
    INPUT_FREQ  : integer := 1;
    OUTPUT_FREQ : integer := 1);
  port (clk_in  : in  std_ulogic;
        clk_out : out std_ulogic);
end;

architecture rtl of clock_divider is
  signal clk : std_ulogic := '0';
begin
  clk_out <= clk;

  process (clk_in)
    constant LIMIT_VAL : integer                       := INPUT_FREQ / 2 / OUTPUT_FREQ;
    variable counter   : positive range 1 to LIMIT_VAL := 1;
  begin
    if clk_in'event and clk_in = '1' then
      if counter = limit_val then
        counter := 1;
        clk     <= not clk;
      else
        counter := counter + 1;
      end if;
    end if;
  end process;

end;
