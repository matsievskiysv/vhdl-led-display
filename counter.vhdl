library ieee;
use ieee.std_logic_1164.all;

entity counter is

  generic (
    MAX : positive := 10);

  port (
    clk, rst : in  std_ulogic;
    count    : out natural range 0 to MAX;
    overflow : out std_ulogic);

end entity counter;

architecture rtl of counter is

begin  -- architecture rtl

  count1 : process (clk, rst) is
    variable count_var : natural range 0 to MAX := 0;
  begin  -- process count1
    if rst = '0' then                   -- asynchronous reset (active low)
      count_var := 0;
      overflow  <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if count_var = MAX then
        count_var := 0;
        overflow  <= '1';
      else
        count_var := count_var + 1;
        overflow  <= '0';
      end if;
    end if;
    count <= count_var;
  end process count1;

end architecture rtl;
