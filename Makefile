GHDL := ghdl
GENERIC ?=
STD ?= 08
GHDL_OPTS ?= -Wall -Werror
WORK ?= work
TOP_UNIT ?= led_display
WAVE = $(TOP_UNIT).ghw
SYM_TIME ?= 200us

YOSYS := yosys
YOSYS_OPTS ?= -Q

NEXTPNR_OPTS ?= --enable-globals --enable-auto-longwires

PACK_OPTS ?=

OPENFPGALOADER := openFPGALoader
OPENFPGALOADER_OPTS :=

ifndef BOARD
$(error BOARD variable not provided. Available options: tangnano4k,tangnano9k)
else ifeq ($(BOARD),tangnano4k)
include tangnano4k.mk
else ifeq ($(BOARD),tangnano9k)
include tangnano9k.mk
else
$(error Unknown BOARD value. Available options: tangnano4k,tangnano9k)
endif

VHDL_SOURCE_FILES := $(filter-out %_tb.vhdl,$(wildcard *.vhdl))
VHDL_TEST_FILES := $(wildcard *_tb.vhdl)
OPTIONS = --work=$(WORK) --std=$(STD) $(GHDL_OPTS) $(foreach g,$(GENERIC),-g$(g))
RUN_OPTIONS = --wave=$(WAVE) --stop-time=$(SYM_TIME)

.DEFAULT_GOAL = check

$(WORK)-obj$(STD).cf: $(VHDL_SOURCE_FILES) $(VHDL_TEST_FILES)
	$(GHDL) import $(OPTIONS) $^

.PHONY: import
import: $(WORK)-obj$(STD).cf

.PHONY: check
check: $(WORK)-obj$(STD).cf
	$(foreach f, $(VHDL_SOURCE_FILES) $(VHDL_TEST_FILES), $(GHDL) syntax $(OPTIONS) $(f);)

.analyze: $(WORK)-obj$(STD).cf
	$(GHDL) make $(OPTIONS) $(TOP_UNIT)
	touch .analyze

.PHONY: analyze
analyze: .analyze

$(WAVE): .analyze
	$(GHDL) run $(OPTIONS) $(TOP_UNIT) $(RUN_OPTIONS)

.PHONY: simulate
simulate: $(WAVE)

.PHONY: view
view: $(WAVE)
	gtkwave $^

$(TOP_UNIT).json : .analyze
	$(YOSYS) $(YOSYS_OPTS) -m ghdl -p 'ghdl -C --std=$(STD) $(WORK).$(TOP_UNIT); $(YOSYS_SYNTH) -json $@'

.PHONY: generate
generate : $(TOP_UNIT).json

pnr-$(TOP_UNIT).json: $(TOP_UNIT).json $(NEXTPNR_CST)
	$(NEXTPNR) $(NEXTPNR_OPTS) --freq $(FREQ) --json $(TOP_UNIT).json --write pnr-$(TOP_UNIT).json --cst $(NEXTPNR_CST) --device $(NEXTPNR_DEVICE)

.PHONY: synth
synth: pnr-$(TOP_UNIT).json

$(TOP_UNIT).fs: pnr-$(TOP_UNIT).json
	$(PACK) $(PACK_OPTS) -d $(PACK_DEVICE) -o $@ $^

.PHONY: pack
pack: $(TOP_UNIT).fs

.PHONY: burn_ram
burn_ram: $(TOP_UNIT).fs
	$(OPENFPGALOADER) $(OPENFPGALOADER_OPTS) --write-sram --board $(OPENFPGALOADER_BOARD) $^

.PHONY: burn_flash
burn_flash: $(TOP_UNIT).fs
	$(OPENFPGALOADER) $(OPENFPGALOADER_OPTS) --reset --write-flash --board $(OPENFPGALOADER_BOARD) $^

.PHONY: clean
clean:
	$(GHDL) --remove $(OPTIONS)
	rm -f $(WORK)-obj$(STD).cf
	rm -f .analyze
	rm -f $(TOP_UNIT).json
	rm -f pnr-$(TOP_UNIT).json
	rm -f $(WAVE)
	rm -f $(UNIT).zip
	find . \( \
	-iname "*.cf" \
	-o -iname "*.fs" \
	-o -iname "*.ghw" \
	-o -iname "*.json" \
	\) -delete

.PHONY: zip
zip:
	zip --symlinks --recurse-paths $(UNIT).zip .
