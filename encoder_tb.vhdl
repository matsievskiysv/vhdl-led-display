library ieee;
use ieee.std_logic_1164.all;
use work.encoder_types.all;

entity encoder_tb is

end entity encoder_tb;

architecture rtl of encoder_tb is

  component encoder is

    port (
      count : in  encoder_t;
      dot   : in  std_ulogic;
      leds  : out std_ulogic_vector(7 downto 0));

  end component encoder;

  component clock_mock is

    generic (
      CLOCK_FREQ : integer);

    port (
      clk_out : out std_ulogic);

  end component clock_mock;

  signal count  : encoder_t                     := 0;
  signal clk    : std_ulogic                    := '1';
  signal dot    : std_ulogic                    := '0';
  signal leds   : std_ulogic_vector(7 downto 0) := (others => '0');

begin  -- architecture rtl

  clock1: component clock_mock
    generic map (
      CLOCK_FREQ => 1000000)
    port map (
      clk_out => clk);

  encoder1 : component encoder
    port map (
      count => count,
      dot   => dot,
      leds  => leds);

  count1 : process (clk) is
    variable counter : encoder_t := 0;
  begin  -- process count1
    if clk'event and clk = '1' then     -- rising clock edge
      if counter = encoder_t'high then
        counter := 0;
        dot     <= not dot;
      else
        counter := counter + 1;
      end if;
    end if;
    count <= counter;
  end process count1;

end architecture rtl;
