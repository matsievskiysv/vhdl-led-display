package encoder_types is

  subtype encoder_t is natural range 0 to 9;

end package encoder_types;

library ieee;
use ieee.std_logic_1164.all;
use work.encoder_types.all;

entity encoder is

  generic (
    LED_COUNT : integer := 8);

  port (
    count : in  encoder_t;
    dot   : in  std_ulogic;
    leds  : out std_ulogic_vector(LED_COUNT-1 downto 0));

end entity encoder;

architecture rtl of encoder is

begin  -- architecture rtl

  leds(LED_COUNT-1) <= dot;

  with count select
    leds(LED_COUNT-2 downto 0) <=
    "0111111" when 0,
    "0000110" when 1,
    "1011011" when 2,
    "1001111" when 3,
    "1100110" when 4,
    "1101101" when 5,
    "1111101" when 6,
    "0000111" when 7,
    "1111111" when 8,
    "1101111" when 9;

end architecture rtl;
