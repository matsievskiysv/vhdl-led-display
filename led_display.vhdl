library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.encoder_types.all;

entity led_display is

  generic (
    LED_COUNT : integer := 8);

  port (
    pin_clk, pin_btn, pin_rst : in  std_ulogic;
    ld1, ld2                  : out std_ulogic_vector(0 to LED_COUNT-1));

end entity led_display;

architecture rtl of led_display is

  signal clk1, clk2, clk3 : std_ulogic := '0';
  signal count1, count2   : encoder_t  := 0;

begin  -- architecture rtl

  clock1 : entity work.clock_divider
    generic map (
      INPUT_FREQ  => 27000000,
      OUTPUT_FREQ => 1)
    port map (
      clk_in  => pin_clk,
      clk_out => clk1);

  counter1 : entity work.counter
    generic map (
      MAX => 9)
    port map (
      clk      => clk1,
      rst      => pin_rst,
      count    => count1,
      overflow => clk2);

  encoder1 : entity work.encoder
    generic map (
      LED_COUNT => LED_COUNT)
    port map (
      count => count1,
      dot   => clk2,
      leds  => ld2);

  counter2 : entity work.counter
    generic map (
      MAX => 9)
    port map (
      clk      => clk2,
      rst      => pin_rst,
      count    => count2,
      overflow => clk3);

  encoder2 : entity work.encoder
    generic map (
      LED_COUNT => LED_COUNT)
    port map (
      count => count2,
      dot   => clk3,
      leds  => ld1);

end architecture rtl;
