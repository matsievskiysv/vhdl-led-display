# VHDL LED Display project

Install toolchain: [INSTALL.md](INSTALL.md)

Two LED displays SC56-116WA:

| LED index  | Pin  |
|:--|:--|
| a  | 7  |
| b  | 6  |
| c  | 4  |
| d  | 2  |
| e  | 1  |
| f  | 9  |
| g  | 10  |
| DP  | 5  |
| GND  | 3, 8  |
